import React from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity } from 'react-native';


function Item({ title, recipedata, nav }) {
    const onPress = () => nav.navigate('Recipe', { recipedata })
    
    return (
        <View style={styles.item}>
            <Text style={styles.title}>{title}</Text>

            <TouchableOpacity style={styles.button} onPress={onPress}>
                <Text>Press here to yours recipe</Text>
            </TouchableOpacity>
        </View>
    );
}


export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
        };

    }

    componentDidMount() {
        const url = 'https://food1-f5c55.firebaseio.com/.json'
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({ list: responseJson })
            })
    }
    render() {
        const DATA = this.state.list
        const { navigation } = this.props



        return (

            <View style={styles.container}>
                <FlatList
                    data={DATA}
                    renderItem={({ item }) => <Item title={item.name} recipedata={item} nav={navigation} />}
                    keyExtractor={item => item.id}
                />
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E59866',
        alignItems: 'center',
        justifyContent: 'center',
    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
    card: {
        backgroundColor: '#f9c2ff'
    },
    button: {
        backgroundColor: '#C79C41',
        padding: 10,
        margin: 10
    }
});