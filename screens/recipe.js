import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default class ReceipeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = { ingredients: [] }
    }
    componentDidMount() {
        this.setState({ ingredients: this.props.route.params.recipedata.ingredients })
    }
    render() {
        const recipe = this.props.route.params.recipedata
        console.log('this.state' + this.state.ingredients)
        return (
            <View style={styles.container}>
                <Image
                    style={styles.image}
                    source={{ uri: recipe.photo }} />
                <Text style={styles.textbold}>Food Name: {<Text style={styles.text}>{recipe.name}</Text>}</Text>
                <Text style={styles.textbold}>Intro: {<Text style={styles.text}>{recipe.short}</Text>}</Text>
                    <View style={{ backgroundColor: 'white' }}>
                        {this.state.ingredients.map((item, key) => {
                            return <View key={key}><Text style={styles.textbold}>Ingredients: {<Text style={styles.text}>{item.name}: {item.amount} {item.unit}</Text>}
                            </Text></View>
                    })
                    }

                </View>
                <Text style={styles.textbold}>How to make: {<Text style={styles.text}>{recipe.long}</Text>}</Text>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E59866',


    },
    image: {
        height: '20%',
        width: '100%'
    },
    textbold:{
        backgroundColor: '#E59866',
        fontWeight: "bold",
        color: '#191970'
    },
    text:{
        backgroundColor: '#E59866',
        fontWeight: "normal",
        color: '#191970'
    },
});