import React from 'react';
import { StyleSheet, Text, View,Image } from 'react-native';

export default class SearchScreen extends React.Component {
    render(){
        return(
        <View style={styles.container}>
            <Image style={styles.image} 
                source = {{uri:'https://cdn.discordapp.com/attachments/711948593833050194/712622630812909588/splash.png'}}
            /> 
            <Text style={styles.text}>Welcome to Shruth Aharn</Text>
        </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#E59866',
      alignItems: 'center',
      justifyContent: 'center',
    },
    image:{
        height: '75%',
        width: '100%'
    },
    text:{
        fontWeight: "bold",
        color: '#7BD41C'
    }
  });