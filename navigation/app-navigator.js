import React from 'react';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import HomeScreen from '../screens/home';
import WelcomeScreen from '../screens/welcome';
import RecipeScreen from '../screens/recipe';
import ProfileScreen from '../screens/profile';

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

function MainTabNavigator() {
    return (
        
            <Tab.Navigator

                activeColor="#f0edf6"
                inactiveColor="#3e2465"
                barStyle={{ backgroundColor: '#FFA500' }}>

                
                <Tab.Screen name="Welcome" component={WelcomeScreen}
                    options={{
                        tabBarLabel: 'Welcome',
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name="hand" color={color} size={30} />
                        ),
                    }} />
                <Tab.Screen name="Home" component={HomeScreen}
                    options={{
                        tabBarLabel: 'Home',
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name="home" color={color} size={30} />
                        ),
                    }} />
                <Tab.Screen name="Profile" component={ProfileScreen}
                    options={{
                        tabBarLabel: 'Profile',
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name="account" color={color} size={30} />
                        ),
                    }} />
            </Tab.Navigator>
        
    )
}
function MainStackNavigator(){
    return(
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name='Home'
                    component={MainTabNavigator}
                    options={{ title: 'Shruth Aharn' }}
                />
                <Stack.Screen
                    name='Recipe'
                    component={RecipeScreen}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )

}

export default MainStackNavigator









