import React from 'react';
import { StyleSheet, Text, View,Image } from 'react-native';

export default class ProfileScreen extends React.Component {
    render(){
        return(
        <View style={styles.container}>
            <Image style={styles.image} 
                source = {{uri:'https://cdn.discordapp.com/attachments/711948593833050194/712375045988548749/Me.jpg'}}
            />   
        <Text style={styles.textbold}>Name: {<Text style={styles.text}>Saranpong Maniphatworadet</Text>}</Text>
        <Text style={styles.textbold}>Balance: {<Text style={styles.text}>100000</Text>}</Text>
        <Text style={styles.textbold}>Favorite: {<Text style={styles.text}>Fried</Text>}</Text>
        <Text style={styles.textbold}>Not Prefer: {<Text style={styles.text}>Insest</Text>}</Text>
        </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexShrink: 2,
      backgroundColor: '#41C3C7',
    },
    textbold:{
        fontWeight: "bold",
        color: '#6D7485'
    },
    text:{
        fontWeight: "normal",
        color: '#5E76AD'
    },
    image:{
        height: '50%',
        width: '50%'
    }
  });